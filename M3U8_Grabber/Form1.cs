﻿using M3U8_Grabber.Properties;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace M3U8_Grabber
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void changeDirButton_MouseClick(object sender, MouseEventArgs e)
        {
            using(var saveDialog = new SaveFileDialog())
            {
                //saveDialog.Filter = "MP4 (*.mp4)|*.mp4|All Files (*.*)|*.*";
                saveDialog.Filter = "mp4 (*.mp4)|*.mp4|webm (*.webm)|*.webm|flv (*.flv)|*.flv|mkv (*.mkv)|*.mkv|ts (*.ts)|*.ts|All Files (*.*)|*.*";
                saveDialog.DefaultExt = "mp4";
                if(Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)))
                {
                    saveDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                try
                {
                    DialogResult result = saveDialog.ShowDialog(new Form1());
                    if(result == DialogResult.OK)
                    {
                        fileLocationTextBox.Text = saveDialog.FileName;
                        var filename = saveDialog.FileName.Split('\\').Last();
                        fileNameTextBox.Text = filename;
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Save dialog error: " + ex.Message);
                }
            }
        }

        private void fileNameTextBox_TextChanged(object sender, EventArgs e)
        {
            string path = fileLocationTextBox.Text.Substring(0, fileLocationTextBox.Text.LastIndexOf(@"\") + 1);
            fileLocationTextBox.Text = path + fileNameTextBox.Text;
        }

        private void fileLocationTextBox_TextChanged(object sender, EventArgs e)
        {
            fileNameTextBox.Text = fileLocationTextBox.Text.Split('\\').Last();
        }

        private void downloadButton_MouseClick(object sender, MouseEventArgs e)
        {
            ProgressBar.Value = 0;
            string url = m3u8TextBox.Text;
            string outputFile = fileLocationTextBox.Text;
            if(!String.IsNullOrEmpty(url) && !String.IsNullOrEmpty(outputFile))
            {
                ffmpegAction ffaction = new ffmpegAction();
                string cmdText = string.Format("-threads 0 -i \"{0}\" -c copy -y -bsf:a aac_adtstoasc -movflags +faststart \"{1}\" {2}", url, outputFile, cmdLineFlagsTextBox.Text);
                try
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.CreateNoWindow = true;
                    startInfo.UseShellExecute = false;
                    startInfo.FileName = "ffmpeg.exe";
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    startInfo.RedirectStandardOutput = true;
                    Process.Start(startInfo);
                    ffaction.getFile("ffmpeg.exe", cmdText, this);
                }
                catch(Exception)
                {
                    if(File.Exists(@".\ffmpeg.exe"))
                    {
                        ffaction.getFile(@".\ffmpeg.exe", cmdText, this);
                    }
                    else
                    {
                        CopyResource(@".\ffmpeg.exe");
                        ffaction.getFile(@".\ffmpeg.exe", cmdText, this);
                    }
                }
            }
        }

        private void CopyResource(string file)
        {
            //if (!Directory.Exists(file.Split('\\').First()))
            //{
            //    Directory.CreateDirectory(file.Split('\\').First());
            //}
            using(FileStream resource = new FileStream(file, FileMode.Create, FileAccess.Write))
            {
                if(resource == null)
                {
                    throw new ArgumentException("No such resource", "resourceName");
                }
                resource.Write(Resources.ffmpeg, 0, Resources.ffmpeg.Length);
            }
        }
    }
}