﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace M3U8_Grabber
{
    public class ffmpegAction : Form1
    {
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MOVE = 0xF010;
        public const int HTCAPTION = 0x0002;

        [DllImport("kernel32.dll")]
        private static extern bool GenerateConsoleCtrlEvent(int dwCtrlEvent, int dwProcessGroupId);

        [DllImport("kernel32.dll")]
        private static extern bool SetConsoleCtrlHandler(IntPtr handlerRoutine, bool add);

        [DllImport("kernel32.dll")]
        private static extern bool AttachConsole(int dwProcessId);

        [DllImport("kernel32.dll")]
        private static extern bool FreeConsole();

        [DllImport("user32.dll")]
        public static extern bool FlashWindow(IntPtr hWnd, bool bInvert);

        private static int ffmpegid;
        private Process cmdProcess;
        //public bool InvokeRequired { get; private set; }

        private static Form1 form;

        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void getFile(string fileName, string ffmpegArgs, Form1 form1)
        {
            form = form1;
            form.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            cmdProcess = new Process();
            form.ffmpegOutput.Text = "";
            cmdProcess.StartInfo.FileName = fileName;
            cmdProcess.StartInfo.Arguments = ffmpegArgs;

            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.RedirectStandardInput = true;

            cmdProcess.StartInfo.RedirectStandardOutput = true;
            cmdProcess.OutputDataReceived += new DataReceivedEventHandler(dataReceived);
            cmdProcess.StartInfo.RedirectStandardError = true;
            cmdProcess.ErrorDataReceived += new DataReceivedEventHandler(errorReceived);

            cmdProcess.EnableRaisingEvents = true;
            cmdProcess.Exited += new EventHandler(cmdProcess_Exited);

            cmdProcess.Start();
            ffmpegid = cmdProcess.Id;
            cmdProcess.BeginOutputReadLine();
            cmdProcess.BeginErrorReadLine();
            form.downloadButton.Visible = false;
            form.stopDownloadBtn.Visible = true;
            form.stopDownloadBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.stopDownloadBtn_MouseClick);
            form.ffmpegOutput.ScrollBars = ScrollBars.Vertical;
            form.ffmpegOutput.TextChanged += new System.EventHandler(ffmpegOutputChanged);
            form.ProgressBar.Visible = true;
            form.durationLabel.Visible = true;
            form.downloadedLabel.Visible = true;
        }

        public static String FormatFileSize(Double fileSize)
        {
            if(fileSize < 0)
            {
                throw new ArgumentOutOfRangeException("fileSize");
            }
            else if(fileSize >= 1024 * 1024 * 1024)
            {
                return string.Format("{0:########0.00} GB", ((Double)fileSize) / (1024 * 1024 * 1024));
            }
            else if(fileSize >= 1024 * 1024)
            {
                return string.Format("{0:####0.00} MB", ((Double)fileSize) / (1024 * 1024));
            }
            else if(fileSize >= 1024)
            {
                return string.Format("{0:####0.00} KB", ((Double)fileSize) / 1024);
            }
            else
            {
                return string.Format("{0} bytes", fileSize);
            }
        }

        private static void changeProgress()
        {
            try
            {
                Double All = Convert.ToDouble(Convert.ToDouble(form.durationLabel.Text.Substring(10, 2)) * 60 * 60 +
                    Convert.ToDouble(form.durationLabel.Text.Substring(13, 2)) * 60 +
                    Convert.ToDouble(form.durationLabel.Text.Substring(16, 2)) +
                    Convert.ToDouble(form.durationLabel.Text.Substring(19, 2)) / 100);

                Double Downloaded = Convert.ToDouble(Convert.ToDouble(form.downloadedLabel.Text.Substring(12, 2)) * 60 * 60 +
                    Convert.ToDouble(form.downloadedLabel.Text.Substring(15, 2)) * 60 +
                    Convert.ToDouble(form.downloadedLabel.Text.Substring(18, 2)) +
                    Convert.ToDouble(form.downloadedLabel.Text.Substring(21, 2)) / 100);

                if(All == 0) All = 1;
                Double Progress = (Downloaded / All) * 100;
                if(Progress > 100) Progress = 100;
                if(Progress < 0) Progress = 0;
                form.ProgressBar.Value = Convert.ToInt32(Progress);
                if(Progress == 100) form.ProgressBar.Visible = false;
            }
            catch(Exception)
            {
                form.ProgressBar.Visible = false;
            }
        }

        private void ffmpegOutputChanged(object sender, EventArgs e)
        {
            Regex duration = new Regex(@"Duration: (\d\d[.:]){3}\d\d", RegexOptions.Compiled | RegexOptions.Singleline);
            form.durationLabel.Text = "Duration: " + duration.Match(form.ffmpegOutput.Text).Value.Replace("Duration: ", "");
            Regex regex = new Regex(@"(\d\d[.:]){3}\d\d", RegexOptions.Compiled | RegexOptions.Singleline);
            var time = regex.Matches(form.ffmpegOutput.Text);
            Regex size = new Regex(@"[1-9][0-9]{0,}kB time", RegexOptions.Compiled | RegexOptions.Singleline);
            var sizekb = size.Matches(form.ffmpegOutput.Text);
            if(time.Count > 0 && sizekb.Count > 0)
            {
                form.downloadedLabel.Text = "Downloaded: " + time.OfType<Match>().Last() + "，" + FormatFileSize(Convert.ToDouble(sizekb.OfType<Match>().Last().ToString().Replace("kB time", "")) * 1024);
                changeProgress();
            }
        }

        private static void dataOutput(string outLine)
        {
            // Add the text to the collected output.
            form.ffmpegOutput.AppendText(outLine + "  " + Environment.NewLine);
            form.ffmpegOutput.VisibleChanged += (sender, e) =>
            {
                if(form.ffmpegOutput.Visible)
                {
                    form.ffmpegOutput.SelectionStart = form.ffmpegOutput.TextLength;
                    form.ffmpegOutput.ScrollToCaret();
                }
            };
        }

        private static void dataReceived(object sendingProcess, DataReceivedEventArgs outLine)
        {
            // Collect the net view command output.
            if(!String.IsNullOrEmpty(outLine.Data))
            {
                if(form.ffmpegOutput.InvokeRequired == true)
                {
                    dataOutput(outLine.Data);
                }
                else
                {
                    dataOutput(outLine.Data);
                }
            }
        }

        private static void errorOutput(string errLine)
        {
            form.ffmpegOutput.AppendText(errLine + "  " + Environment.NewLine);
            form.ffmpegOutput.VisibleChanged += (sender, e) =>
            {
                if(form.ffmpegOutput.Visible)
                {
                    form.ffmpegOutput.SelectionStart = form.ffmpegOutput.TextLength;
                    form.ffmpegOutput.ScrollToCaret();
                }
            };
        }

        private static void errorReceived(object sendingProcess, DataReceivedEventArgs errLine)
        {
            // Write the error text to the file if there is something
            // to write and an error file has been specified.
            if(!String.IsNullOrEmpty(errLine.Data))
            {
                if(form.ffmpegOutput.InvokeRequired == true)
                {
                    form.ffmpegOutput.Invoke((MethodInvoker)delegate
                    {
                        errorOutput(errLine.Data);
                    });
                }
                else
                {
                    errorOutput(errLine.Data);
                }
            }
        }

        private void setComplete()
        {
            string msg;
            if(form.ProgressBar.Value == 100)
            {
                msg = "Download Complete!";
                MessageBox.Show(msg, "M3U8 Downloader", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                form.durationLabel.Text = "Download Complete";
                form.stopDownloadBtn.Visible = false;
                form.downloadButton.Visible = true;
            }
            else
            {
                msg = "Process Ended";
                MessageBox.Show(msg, "M3U8 Downloader", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                form.stopDownloadBtn.Visible = false;
                form.downloadButton.Visible = true;
            }
            form.ProgressBar.Visible = false;
        }

        private void cmdProcess_Exited(object sender, System.EventArgs e)
        {
            //FlashWindow(form.Handle, true);
            if(form.InvokeRequired == true)
            {
                form.Invoke((MethodInvoker)delegate
                {
                    setComplete();
                });
            }
            else
            {
                setComplete();
            }
        }

        public void Stop()
        {
            AttachConsole(ffmpegid);
            SetConsoleCtrlHandler(IntPtr.Zero, true);
            GenerateConsoleCtrlEvent(0, 0);
            FreeConsole();
        }

        private void stopDownloadBtn_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                form.ProgressBar.Visible = false;
                form.ProgressBar.Value = 0;
                form.durationLabel.Visible = false;
                form.downloadedLabel.Visible = false;
                form.stopDownloadBtn.Visible = false;
                form.downloadButton.Visible = true;
                if(Process.GetProcessById(ffmpegid) != null)
                {
                    try
                    {
                        if(Process.GetProcessById(ffmpegid) != null)
                        {
                            Process.GetProcessById(ffmpegid).Kill();
                            Dispose();
                        }
                    }
                    catch
                    {
                        Dispose();
                    }
                }
            }
            catch(Exception) { }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                var testProc = Process.GetProcessById(ffmpegid);
            }
            catch(Exception)
            {
                return;
            }
            if(Process.GetProcessById(ffmpegid) != null)
            {
                try
                {
                    //Process.GetProcessById(ffmpegid).Kill();
                    Stop();
                    Dispose();
                }
                catch
                {
                    Dispose();
                }
            }
        }
    }
}