﻿namespace M3U8_Grabber
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.durationLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.downloadedLabel = new System.Windows.Forms.ToolStripLabel();
            this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.changeDirButton = new System.Windows.Forms.Button();
            this.fileLocationTextBox = new System.Windows.Forms.TextBox();
            this.stopDownloadBtn = new System.Windows.Forms.Button();
            this.downloadButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m3u8TextBox = new System.Windows.Forms.TextBox();
            this.ffmpegOutput = new System.Windows.Forms.TextBox();
            this.cmdLineFlagsTextBox = new System.Windows.Forms.TextBox();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.groupBox1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.stopDownloadBtn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.downloadButton);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label3);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label2);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.m3u8TextBox);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.ffmpegOutput);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.cmdLineFlagsTextBox);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1126, 414);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1126, 464);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.durationLabel,
            this.toolStripSeparator1,
            this.downloadedLabel,
            this.ProgressBar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(1126, 25);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 0;
            // 
            // durationLabel
            // 
            this.durationLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.durationLabel.Name = "durationLabel";
            this.durationLabel.Size = new System.Drawing.Size(70, 22);
            this.durationLabel.Text = "Duration:";
            this.durationLabel.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // downloadedLabel
            // 
            this.downloadedLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.downloadedLabel.Name = "downloadedLabel";
            this.downloadedLabel.Size = new System.Drawing.Size(98, 22);
            this.downloadedLabel.Text = "Downloaded:";
            this.downloadedLabel.Visible = false;
            // 
            // ProgressBar
            // 
            this.ProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(200, 22);
            this.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.ProgressBar.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.fileNameTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.changeDirButton);
            this.groupBox1.Controls.Add(this.fileLocationTextBox);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.groupBox1.Location = new System.Drawing.Point(45, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(319, 232);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fileNameTextBox.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.fileNameTextBox.Location = new System.Drawing.Point(16, 115);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(282, 27);
            this.fileNameTextBox.TabIndex = 6;
            this.fileNameTextBox.TextChanged += new System.EventHandler(this.fileNameTextBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.label5.Location = new System.Drawing.Point(15, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "File name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.label4.Location = new System.Drawing.Point(15, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Save to:";
            // 
            // changeDirButton
            // 
            this.changeDirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeDirButton.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.changeDirButton.Location = new System.Drawing.Point(16, 164);
            this.changeDirButton.Name = "changeDirButton";
            this.changeDirButton.Size = new System.Drawing.Size(282, 44);
            this.changeDirButton.TabIndex = 8;
            this.changeDirButton.Text = "Select Directory";
            this.changeDirButton.UseVisualStyleBackColor = true;
            this.changeDirButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.changeDirButton_MouseClick);
            // 
            // fileLocationTextBox
            // 
            this.fileLocationTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fileLocationTextBox.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.fileLocationTextBox.Location = new System.Drawing.Point(16, 49);
            this.fileLocationTextBox.Name = "fileLocationTextBox";
            this.fileLocationTextBox.Size = new System.Drawing.Size(285, 27);
            this.fileLocationTextBox.TabIndex = 5;
            this.fileLocationTextBox.TextChanged += new System.EventHandler(this.fileLocationTextBox_TextChanged);
            // 
            // stopDownloadBtn
            // 
            this.stopDownloadBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stopDownloadBtn.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.stopDownloadBtn.Location = new System.Drawing.Point(61, 358);
            this.stopDownloadBtn.Name = "stopDownloadBtn";
            this.stopDownloadBtn.Size = new System.Drawing.Size(282, 42);
            this.stopDownloadBtn.TabIndex = 4;
            this.stopDownloadBtn.Text = "Stop Download";
            this.stopDownloadBtn.UseVisualStyleBackColor = true;
            this.stopDownloadBtn.Visible = false;
            // 
            // downloadButton
            // 
            this.downloadButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.downloadButton.Location = new System.Drawing.Point(61, 358);
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.Size = new System.Drawing.Size(282, 42);
            this.downloadButton.TabIndex = 4;
            this.downloadButton.Text = "Start Download";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.downloadButton_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.label3.Location = new System.Drawing.Point(60, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "URL:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.label2.Location = new System.Drawing.Point(385, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Output:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.label1.Location = new System.Drawing.Point(385, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Command Line Switches:";
            // 
            // m3u8TextBox
            // 
            this.m3u8TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m3u8TextBox.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.m3u8TextBox.Location = new System.Drawing.Point(61, 57);
            this.m3u8TextBox.Name = "m3u8TextBox";
            this.m3u8TextBox.Size = new System.Drawing.Size(286, 27);
            this.m3u8TextBox.TabIndex = 0;
            // 
            // ffmpegOutput
            // 
            this.ffmpegOutput.BackColor = System.Drawing.Color.White;
            this.ffmpegOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ffmpegOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ffmpegOutput.Location = new System.Drawing.Point(388, 110);
            this.ffmpegOutput.Multiline = true;
            this.ffmpegOutput.Name = "ffmpegOutput";
            this.ffmpegOutput.ReadOnly = true;
            this.ffmpegOutput.Size = new System.Drawing.Size(692, 290);
            this.ffmpegOutput.TabIndex = 6;
            // 
            // cmdLineFlagsTextBox
            // 
            this.cmdLineFlagsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdLineFlagsTextBox.Font = new System.Drawing.Font("Microsoft YaHei", 9F);
            this.cmdLineFlagsTextBox.Location = new System.Drawing.Point(388, 57);
            this.cmdLineFlagsTextBox.Name = "cmdLineFlagsTextBox";
            this.cmdLineFlagsTextBox.Size = new System.Drawing.Size(692, 27);
            this.cmdLineFlagsTextBox.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 464);
            this.Controls.Add(this.toolStripContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "M3U8 Grabber";
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.TextBox m3u8TextBox;
        internal System.Windows.Forms.TextBox ffmpegOutput;
        private System.Windows.Forms.TextBox cmdLineFlagsTextBox;
        internal System.Windows.Forms.Button downloadButton;
        internal System.Windows.Forms.Button stopDownloadBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        internal System.Windows.Forms.ToolStripLabel durationLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripLabel downloadedLabel;
        internal System.Windows.Forms.ToolStripProgressBar ProgressBar;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button changeDirButton;
        private System.Windows.Forms.TextBox fileLocationTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

