﻿using System;
using System.Windows.Forms;

namespace M3U8_Grabber
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Application Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);
            }
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            DialogResult result = ShowThreadExceptionDialog(e.Exception);

            // Exit the program when the user clicks Abort.
            if (result == DialogResult.Abort)
                Application.Exit();
        }

        // Create and display the error message.
        private static DialogResult ShowThreadExceptionDialog(Exception e)
        {
            string errorMsg = "An error occurred.  Please contact the adminstrator " +
                 "with the following information:\n\n";
            errorMsg += String.Format("Exception Type: {0}\n\n", e.GetType().Name);
            errorMsg += "\n\nStack Trace:\n" + e.StackTrace;
            return MessageBox.Show(errorMsg, "Application Error",
                 MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);
        }
    }
}