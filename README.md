### <img src="https://gitlab.com/applications2/m3u8_grabber/raw/master/M3U8_Grabber/Resources/Download-Button-icon.ico"  width="20" height="20" align="middle"> M3U8_Grabber
* [![CSharp badge](https://img.shields.io/badge/Made%20with-CSharp-6a5acd.svg)](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/)
* [![DotNet badge](https://img.shields.io/badge/dotnet-v4.7.2-orange)](https://dotnet.microsoft.com/download/dotnet-framework/net472)
* This utility facilitates grabbing media files with a m3u8 file (works with mp4, webm, and other files).
* Wrapper for ffmpeg.exe.
----
#### <img src="https://gitlab.com/applications2/m3u8_grabber/raw/master/M3U8_Grabber/Resources/Download-Button-icon.ico"  width="20" height="20" align="middle"> M3U8_Granner - Information Section
* This utility:
    * Is a wrapper for ffmpeg.exe.
    * Facilitates grabbing media files with a m3u8 file (works with mp4, webm, and other files).
    * Includes a text box for command line flags, but they are not required to download media.
        * Example flags:
            * ```-user_agent "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36 OPR/65.0.3467.42"```
            * ```-headers "Referer: https://whatever.website.org"```
        * Any command line flag from the ffmpeg documentation page can be use (https://ffmpeg.org/ffmpeg.html).
    * The ffmpeg.exe executable is held in the application's resource manager.
        * Upon exectution, this application checks whether ffmpeg.exe is already in the system PATH. If so, no extraction takes place.
        * Then it checks the directory in which this compiled application is located.
        * If ffmpeg.exe is not already on the system, found in the system PATH, or in the same location of this compiled application, it is extracted  from resource manager and places in the same directory of this application.
        * To upgrade ffmpeg.exe included in this project, replace the one located in resource manager.
        * To upgrade ffmpeg.exe without changing the project or recompiling, add a system PATH to the new ffmpeg.exe or place ffmpeg.exe (overwrite) in the same location as this compiled application. 